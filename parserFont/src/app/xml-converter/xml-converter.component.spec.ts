import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XmlConverterComponent } from './xml-converter.component';

describe('XmlConverterComponent', () => {
  let component: XmlConverterComponent;
  let fixture: ComponentFixture<XmlConverterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XmlConverterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XmlConverterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
